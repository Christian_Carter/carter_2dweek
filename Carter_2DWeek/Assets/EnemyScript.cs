﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] SpriteRenderer sprite;

    [Header("Enemy Speed and Location")]
    [SerializeField] float moveSpeed;
    [SerializeField] float minX;
    [SerializeField] float maxX;
    public bool moveRight = true;
    public GameObject youlose;

    void Start()
    {
        youlose.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            youlose.SetActive(true);
        }
    }

    void Update()
    {
        if (transform.position.x > maxX)
        {
            moveRight = false;
        }
        if (transform.position.x < minX)
        {
            moveRight = true;
        }
        if (moveRight)
        {
            transform.position = new Vector2(transform.position.x + moveSpeed * Time.deltaTime, transform.position.y);
            sprite.flipX = true;
        }
        else
        {
            transform.position = new Vector2(transform.position.x - moveSpeed * Time.deltaTime, transform.position.y);
            sprite.flipX = false;
        }
    }
}

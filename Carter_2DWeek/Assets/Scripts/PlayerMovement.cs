﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpSpeed;

    public Animator animator;

    public SpriteRenderer render;

    //Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        //If the player the jump button:
        if (Input.GetButtonDown("Jump"))
        {
            Debug.Log("Presed Jump");
            //Do a box cat of a very thin box our width directly below us to check if we are on a floor
            int levelMask = LayerMask.GetMask("Level");//Get the layer mask the player can jump from [make sure colliders are to "Level" layer]

            //Parameters: (position of center of box, size of box, direction to cast, layer mask to check)
            if (Physics2D.BoxCast(transform.position, new Vector2(1f, 1f), 0f, Vector2.down, .1f, levelMask))
            {
                Debug.Log("Attempted to Jump");
                Jump();
            }
        }
    }

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
        {
            render.flipX = false;
        }
        else
        if (rB2D.velocity.x < 0)
        {
            render.flipX = true;
        }
        if (Mathf.Abs (horizontalInput) > 0f)
        {
            animator.SetBool("IsWalking", true);
        }
        else
        {
            animator.SetBool("IsWalking", false);
        }
    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpSpeed);
    }
}

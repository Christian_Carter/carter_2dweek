﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController1 : MonoBehaviour
{
    public TextMeshProUGUI score;
    public int keeper = 0;

    public void Damage()
    {
        score.text = "Score: " + keeper;
    }
}


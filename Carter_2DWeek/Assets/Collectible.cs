﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class Collectible : MonoBehaviour
{
    public GameController1 game;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            game.keeper += 1;
            Debug.Log(game.keeper);
            game.Damage();

        }
    }
}

